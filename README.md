**Мой хелп по программам, которые изучаю или нерегулярно пользуюсь**

Строго не судите, делаю для себя как понимаю/умею :)


## Soft
> Здесь лежат проги

1. [Hudini](/Soft/Houdini/houdini.md)
2. [Adobe Audition](/Soft/AudioVideo/AdobeAudition.md)
3. [Adobe Premier](/Soft/AudioVideo/AdobePremiere.md)
4. [Pano2VR](/Soft/AudioVideo/Pano2VR.md)

## Hard
> В этой части железяки

1. [Samsung Galaxy Grand Duos GT-I9082](/Hard/samsungGrandDuos.md)
2. [ WD TV Live!](/Hard/wdTvLive.md)