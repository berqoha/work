Pano2VR = программа создания и редактирования видео/html5 3D панорам. [Официальный сайт + туториал](https://ggnome.com/doc/pano2vr/6/p2vr-tutorials/)

# Примеры
* [Добавление видео в 3D панораму](https://www.youtube.com/watch?v=68sHwOC49Is) - льется фонтан на наподвижной картинке
* [Анимация 3D картинки - движение камерой](https://www.youtube.com/watch?v=O21U3-57dSk)